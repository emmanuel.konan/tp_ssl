Emmanuel Konan & Samuel Hiron
# I introduction
 TP illustre le cours portant sur les protocoles cryptographiques.
# II. Mise en place du service de vidéo-surveillance
Après avoir fait les configurations réseaux pour que tous les appareils puissent se ping, que le flux de la caméra soit visible sur Visu, nous mettons en place le firewall avec les règles iptables.
Le flux de caméra est donc redirigé par firewall sur Visu. Visu n'écoute que le firewall.
Nous pouvons voir que cela marche bien au début de la vidéo 1. Le flux de caméra apparaît bien grâce au routage sur Visu.
Cependant l'attaquant est capable de se connecter sur firewall et de changer les règles de routage pour passer son flux à Visu. C'est ce que nous faisons dans la suite de la vidéo 1.

# III. Mise en place d'une infrastructure à clé publique
Nous créons un  certificat racine auto-signé sur caméra avec cette commande
![[SSL camera openssl certificat.png]]
L'option node permet de ne pas chiffré la clé en sortie

Nous pouvons visualiser le contenu du certificat et de la clé avec ces commandes:

```bash
export OPENSSL_CONF=$PWD/openssl .conf # pour que cela fonctionne
openssl x509 -in cacert.pem -text  # certificat
openssl rsa -in private/cakey.pem -text # key
```
![[SSL Crypto certificat.png]]
![[SSL Crypto key.png]]
# IV. Génération du certificat du serveur
1. générer une paire de clé publique / privée &  une demande de certification
	`openssl req -nodes -newkey rsa:2048 -config ./openssl.cnf -keyout serverkey.pem -out serverreq.pem`
	Comme Common Name ou CN, choisissez server
	![[SSL etape1 certificat.png]]
2.  la certification à proprement parler
	`openssl ca -config ./openssl.cnf -in serverreq.pem -out servercert.pem`![[SSL etape2 certificat.png]]

La différence entre le certificat et la demande de certification est que les informations relatives à l'autorité de certification se sont rajouté. Nous avons notamment le champ `issuer`:
![[SSL certificat demande.jpg]]
sur Caméra il fallait faire ces configurations:
`/usr/bin/openssl req -in serverreq.pem -text` 
`cp ../CA/server* .`

`chmod 600 serverkey.pem` sinon cela ne marche pas (normalement)

# V.  Mise en place d’un tunnel SSL entre Visu et Camera
`ps aux | grep stunnel` puis tuer `sudo kill -9 pid`
`sudo stunnel4 ./stunnel.conf`
![[SSL flux visu camera.png]]
Nous remarquons que le traffic est chiffré entre Caméra et Visu. On ne peut plus reproduire l'attaque. Il faut l'adapter.

Nous avons configuré la machine Attaquant pour qu’elle offre également une connexion TLS.
Nous avons généré une certificat auto-signé sur la machine Attaquant et mis en place le tunnel TLS adéquate.
![[SSL flu camera hacker.png]]
![[SSL attaque chiffré.mp4]]



