# Readme
Pour pouvoir consulté le rapport, il faut d'abord télécharger tout le dépôt, le dézipper et ouvrir le fichier html dans un navigateur.

# Objectif
Le but de ce TP est de mettre en place un tunnel SSL/TLS entre deux machines. TLS anciennement SSL est une
méthode originairement mise en place pour apporter de la sécurité lors des communications web (http); notamment
lors des transactions financières sur internet. Cependant aujoud'hui TLS pour sécuriser d'autres types de
connexions : ldap avec ldaps, ftp avec ftps, ntp avec sntp, etc.